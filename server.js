const express = require('express')
const app = express()


let todos = [
    {
        name : 'Poh' ,
        id : 1
    } ,     
    {
        name : 'Pok' ,
        id : 2
    }
]


app.get('/todos', (req, res) => {
    res.send(todos)

})  // SELECT * From TODO >> app.get('/todos', function)


app.post('/todos', (req, res) => {
    let newTodo = {
        name : 'Read a book' ,
        id : 3
    }
    todos.push(newTodo)
    res.status(201).send()
})  // INSERT INTO TODO


app.listen(3000, () => {
    console.log('TODO API Started at port 3000')
})